<?php
include_once 'src/Controller/BaseController.php';

use Atlassofthu\Controller\BaseController;

/**
 * class DueDateCalculatorController
 *
 * @author Akos Varga <achozion@gmail.com>
 */
class DueDateCalculatorController extends BaseController
{

    protected $minimumUnit = "1 minute";

    protected $day = 24*60;

    protected $hour = 60;

    protected $minute = 1;

    protected $oneWorkingDayInMinutes = 8 * 60;

    protected $holidays = array(
        "12-06",
        "12-25",
        "12-26",
        "01-01",
        "08-20",
        "10-23",
    );

    /**
     * calculate
     *
     * @param string
     * @param string
     *
     * @return string
     */
    public function calculate(string $inputDateString, string $turnaroundTimeString): string
    {

        $submitDatetime = \DateTime::createFromFormat('Y-m-d H:i:s', $inputDateString);
        $turnaroundTimeInMinutes = $this->getTurnaroundTimeInMinutes($turnaroundTimeString);

        $minutesUntilSameDayEnding = $this->getMinutesUntilSameDayEnding($inputDateString);

        if (!$this->checkDateIsBetweenStartAndEndDate($inputDateString)) {
            return "Input date not between the start and end working hours date";
        }

        if ($minutesUntilSameDayEnding > $turnaroundTimeInMinutes ) {
            $intvalSpec = "PT".$turnaroundTimeInMinutes."M";
        } else {
            $workingDays = (integer) $this->calculateWorkingDays($inputDateString, $turnaroundTimeInMinutes);
            $intvalSpec = "P".$workingDays."D";
        }

        $outputDateTime = $submitDatetime->add(new \DateInterval($intvalSpec));

        return $outputDateTime->format('Y-m-d H:i:s');
    }

}
