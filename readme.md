Mellékeltem a szakmai leirást is:

Megoldás:
Egy controller és a hozzá tartozó abstract ősosztály

(megjegyzés:
composer van, de nincs használatban
autoloader van, de nincs használatban
Adatbázis és Entity manager van de nincs használatban)

PHP Unit van

Docker konténer szolgáltatja a környezetet
docker compose fájl mellékeve
docker-compose up -d
login / enter docker:
 docker exec -it icftech-mysql mysql -usf4_user -psf4_pw

A két bemenő paraméter az index.php ban megadhatók:
- $inputDateString  (órában megadható de más időegységben is)
- $turnaroundTimeString  (átfutási idő)
(Input paraméter beviteli formájáról nem tesz említést a dokumentácio ezért a legegyszerűbb formát válaszottam. (Php változó paraméter))

Az átfutási idő ($turnaroundTimeString) megadható a következő módon:
 - percet, 
 - napot, 
 - hetet, 
 - hónapot 
 - és évet 
 is bemenő paraméternek.
 
pl:
$turnaroundTimeString = "20 hour"; // means 20 * 60 minutes

elfogadott még:
$turnaroundTimeString = "1 day"; // means 24 * 60 minutes
$turnaroundTimeString = "3 dafdbdf"; // means 3 day -> 3 * 24 * 60 minutes
$turnaroundTimeString = "3 dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "3dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "dfdb3df"; // means 0 minutes -> 0 minutes
$turnaroundTimeString = "dfdbdf"; // means 0 minutes -> 0 minutes
$turnaroundTimeString = "134"; // means 134 minutes
$turnaroundTimeString = "24 minute"; // means 24 minutes
$turnaroundTimeString = "24 minutes"; // means 24 minutes
$turnaroundTimeString = "1 week"; // means 7 * 24 * 60 minutes
$turnaroundTimeString = "480 minutes" // means 480 minutes


Egyszerűen megjeleníti a képernyőn a céldátumot: $outputDateString néven.

A szabadság időpontjai tetszőlegesen beállítható újak hozzáadásával vagy a régiek elvételével.

Köszönöm a lehetőséget, hogy megoldhattam a tesztfeladatot
Üdvözlettel:Varga Ákos
achozion@gmail.com

-------------------
Atlas Soft PHP teszt feladat
Készíts egy olyan osztályt, amely kiszámolja egy projektre a kezdés dátuma és a feladat átfutási ideje alapján, hogy mikor lesz kész.

Az átfutási idő munkaórában van megadva. Kapjunk értesítést, ha nem munkaidőben van a kezdés dátuma. 
A munkaidő hétfőtől péntekig, 9-től 17 óráig tart, az ünnep- és munkaszüneti napoktól egyelőre tekintsünk el.

A megoldáshoz mellékelj PHPUnit framework segítégével készített pár soros tesztet is.